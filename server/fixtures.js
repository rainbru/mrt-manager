/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// from http://en.wikipedia.org/wiki/FIFA#Six_confederations_and_209_national_associations

var succefully_created = new Array();
var cant_find          = new Array();
var collision          = new Array();

var randomItem = function(arr){
    return arr[Math.floor(Math.random() * arr.length)]
}

//import 'lib/generators/team.js';

// It's time to create a country
var addCountry = function(abbrev){
    console.log("Adding country " + abbrev);
    try {
	// First get the country from the abbrev to add some values
	parent = Geographies.findOne({abbrev: abbrev});
	console.log("Found parent is ");
	console.log(parent)
	
	myjson = JSON.parse(Assets.getText(abbrev + ".json"));
	config = myjson.config; // The country config
	console.log(config);
	console.log("Level name is "+ config['1st-level-name']);

	var created = Meteor.TeamGenerator.newSeason( myjson, parent._id);
	console.log("Correctly created " + created) + " teams";
	
    } catch (err) {
	console.log("Error creating country from " + abbrev + ".json :"
		    + err.message)

    }
}

var addChilds = function(abbrev, name, parent_id){
    var myjson = {};

    // A primitive anti-collision test to avoid two countries using same
    // abbreviation
    if (collision.hasOwnProperty(abbrev)) {
	console.log("Country code collision warning : '" + abbrev
		    + "' code was already claimed by " + collision[abbrev]
		    + " and is now claimed by " + name);
    }
    else
    {
	collision[abbrev] = name;
    }
    
    try {
	// Simply parse the JSON file and throw an error if not found
	myjson = JSON.parse(Assets.getText(abbrev + ".json"));

	if (abbrev[0] != "_"){ // Handles it as a country
	    console.log("No underscore found, "+abbrev+" should be a country");
	    addCountry(abbrev);
	}
	else // Handles it as a geography
	{ 
	    myjson.childs.forEach(function(e){
		
		id = Geographies.insert({
		    label: e.name,
		    abbrev: e.abbr,
		    parent: parent_id
		});
		addChilds(e.abbr, e.name, id);
		succefully_created.push(abbrev);
	    });
	}
    }
    catch (err) {
	if ( err.name === "SyntaxError"){
	    console.log("Syntax error while creating " + abbrev + ".json :"
		       + err.message)
	}
	else{
	    cant_find.push(abbrev);
	}
	
    }
}

if ( Geographies.find().count() === 0 ) {
    addChilds("__FIFA", "World", null);
    console.log("All Geographies loaded");
    console.log("Succefully created are : " +
		succefully_created.filter( unique ).sort().join());
    console.log("You have to create them : " +
		cant_find.filter( unique ).sort().join());
}

if ( Generators.find().count() === 0 ) {
    Generators.insert({
        label: "World cup",
	parent: null,
	periodicity: 4,
	input: "NationalTeams"
    });
}

if ( News.find().count() === 0 ) {
    News.insert({
	title: "Exampel news",
	type: 0,
	visibility: 1,
	content: "This is an example news content"
    });
}

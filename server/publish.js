/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Give authorized users access to sensitive data by group

Meteor.publish('secrets', function (group) {
    if (Roles.userIsInRole(this.userId, ['view-secrets','admin'], group)) {
	return Meteor.secrets.find({group: group});
    } 
    else {
	// user not authorized. do not publish secrets
	this.stop();
	return;
    }
});

Meteor.publish("fnames", function () {
    return Firstnames.find();
});

Meteor.publish("lnames", function () {
    return Lastnames.find();
});

Meteor.publish("allgeographies", function () {
    return Geographies.find();
});

Meteor.publish("continents", function () {
    return Geographies.find({parent: null});
});

Meteor.publish("generators", function () {
    return Generators.find({});
});

Meteor.publish("geography", function (id) {
    return Geographies.find({_id: id});
});

Meteor.publish("geoChilds", function (id) {
    return Geographies.find({parent: id});
});

Meteor.publish("geoParent", function (id) {
    var g = Geographies.findOne(id);
    if (g === undefined || g.parent === null){
	return [];
    }
    else{
	return Geographies.find({_id: g.parent});
    }
});

Meteor.publish("news", function () {
    return News.find({});
});

Meteor.publish("teams", function () {
    return Teams.find({});
});

Meteor.publish("players", function () {
    return Players.find({});
});

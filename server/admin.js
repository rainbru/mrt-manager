/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

Meteor.startup(function () {
    // Try to get values from settings.json
    if (Meteor.settings.hasOwnProperty('private')){
	if (Meteor.settings.private.hasOwnProperty('admin')){
	    var admin = Meteor.settings.private.admin;

	    var _email   = admin.email;
	    var _pwd     = admin.password;
	    var _profile = admin.profile;

	    // Search if the admin user already exists
	    var e = Meteor.users.find({ 
		emails: { $elemMatch: { address: _email } } 
	    }).count();

	    // Need to create an admin account
	    if (e < 1){
		id = Accounts.createUser({
		    email:    _email,
		    password: _pwd,
		    profile: { name: _profile },
		});
	
		Roles.addUsersToRoles(id, ['admin']);
		console.log("Correctly created '" + _email + "' admin user");
	    }
	}
    }
    else {
	console.log("Can't create admin account, did you used the --settings flag ?");
    }
});

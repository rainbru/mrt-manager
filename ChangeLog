2018-05-02  Jérôme Pasquier  <rainbru@free.fr>

	* client/helpers/childs.js (getChildsNumber): add new helper.
	* client/stylesheets/news.css: fix title news gradient.

2018-04-24  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/dashboard.html: print admin news.
	* client/templates/profile.html: print typed news for logged in users.

2018-02-23  Jérôme Pasquier  <rainbru@free.fr>

	* lib/string.js (capitalizeFirstLetter): do not modify native prototype
	anymore.
	* .meteor/packages: Try cultofcoders:mocha.
	* .meteor/packages: try meteortesting:mocha package to fix CI.
	* .meteor/versions: update meteor packages.

2018-02-12  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js: fix the updated news id.
	* client/templates/admin/news.js: correctly get date when modifying
	news.
	* client/templates/admin/news.js: fix update button id.
	* client/templates/admin/news.js: add a confirmation before deleting a
	news.
	* .meteor/versions: add @babel/runtime trying to fix CI.
	* .meteor/versions: update to latest meteor version.

2018-01-11  Jérôme Pasquier  <rainbru@free.fr>

	* .meteor/versions: update meteor packages.

2017-10-18  Jérôme Pasquier  <rainbru@free.fr>

	* ROADMAP: submitted r11 to coverity : no defect.

	* lib/news.app-test.js (visibilityToString): implement a news.js test.

	* lib/news.app-test.js (typeToString): implement a news.js test.

	* lib/player.app-test.js (getRandomHeight): implement player.js UT.

	* .meteor/versions: update meteor packages.

2017-09-22  Jérôme Pasquier  <rainbru@free.fr>

	* lib/random.js: uses export to fix undefined issues.
	* lib/random.test.js: add unit tests for lib/random.s.

	* Makefile (coverage): add --full-app to fix partial coverage issue.
	* README.md: add codecov.io badge.
	* .travis.yml (after_success): remove debug commands.

	* package.json (dependencies): remove "spacejam": "^1.6.1". Only keep
	the serut version.

	* .travis.yml (after_success): add debug commands.
	* Makefile (coverage): remove trace loglevel.

	* .travis.yml (after_success): handle codecov.io
	* package.json: install codecov.io npm module
	* test/settings.coverage.json: remove uneeded file.
	* .meteo/packages: add lmieulet:meteor-coverage.

2017-09-21  Jérôme Pasquier  <rainbru@free.fr>

	* Makefile (coverage): testing a new rule.
	* test/settings.coverage.json: add a coverage-related config file.

2017-09-07  Jérôme Pasquier  <rainbru@free.fr>

	* .travis.yml (node_js): remove v4 (too old and fail).

	* Makefile: rename check to cli-check an use spacejam.

	* client/helpers/errors.js (throwError): make unit tests pass.

	* .meteo/rpackages: add ecmascript and practicalmeteor:chai.
	* Makefile (webcheck): testing a new version. Conserve `check` for CI.

2017-09-07  Jérôme Pasquier  <rainbru@free.fr>

	* Makefile (console-check): add a rule for CI to mainly run tests
	in real browser.

	* client/helpers/errors.test.js: some mocha tests.

	* .travis.yml (node_js): change version from 0.12 to 4,6,8.
	* .meteor/versions: update meteor packages.

2017-08-23  Jérôme Pasquier  <rainbru@free.fr>

	* ROADMAP: about to find a unit tests ramework, close the section.

	* .travis.yml (dist): try to install on trusty.

	* .travis.yml (before_install): add build-essential packages.
	* package.json (spacejam): change needed version from 3.10.10 to 1.6.1.

	* .travis.yml (script): change to `make check`.
	* package.json (devDependencies): removed old backage.

	* client/sample.test.js: add an empty test.
	* README.md: add unit tests instructions.
	* .Makefile: add check rule
	* .meteor/versions: add practicalmeteor:mocha.
	* .meteor/versions: update meteor packages.

2017-08-06  Jérôme Pasquier  <rainbru@free.fr>

	* README.md: add a Troubleshooting section fro the mongodb/locale issue.
	* .meteor/versions: update meteor packages.

2017-07-15  Jérôme Pasquier  <rainbru@free.fr>

	* .meteor/versions: update meteor packages.

2017-07-13  Jérôme Pasquier  <rainbru@free.fr>

	* .meteor/versions: update meteor packages.

2017-07-12  Jérôme Pasquier  <rainbru@free.fr>

	* .meteor/versions: update meteor packages.

2017-05-18  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js: started to update modified news.

2017-05-09  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js: save and shows author and date.
	* client/templates/admin/news.js: correctly preselect type combo.
	* .meteor/versions: update meteor packages.

2017-02-23  Jérôme Pasquier  <rainbru@free.fr>

	* .meteor/versions: update meteor packages.

2017-01-10  Jérôme Pasquier  <rainbru@free.fr>

	* tests/: change headers date for 2017.
	* server/: change headers date for 2017.

	* lib/: change headers date for 2017.
	* features/: change headers date for 2017.
	* client/: change headers date for 2017.

2016-12-09  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js (click .modify): trying to preselect
	type anohter way.

	* client/templates/admin/news.js (newsTypes): find a way to preselect
	type from associative array.
	* package.json: add babel-runtime package to fix issue due to update.
	* .meteor/versions: update meteor packages.

2016-11-21  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js (modifyNews newsType): feed the
	type combo for modify dialog.
	* .meteor/versions: updated to Meteor 1.4.2.3.

2016-11-08  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js (modifyNews click .modify): start
	to prefill modal with colelction values (title and content).

	* client/templates/admin/news.js (modifyNews click .modify): show
	the modify news dialog.
	* client/templates/admin/news.html (modifyNews): add a modify news
	modal.

	* lib/news.js (visibilityToString): make sure parameter in an integer.
	* lib/news.js (typeToString): make sure parameter in an integer.
	* client/templates/admin/news.js (click #add): use parseInt to
	force the typeof saved news type and visibility.

	* client/templates/admin/news.js: deleting a news works.
	* .meteor/versions: update meteor packages.

2016-11-01  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js (news): use lib/news.js to correctly
	print news array content.
	* lib/news.js (visibilityToString): news helper function.
	* lib/news.js (typeToString): news helper function.

2016-10-25  Jérôme Pasquier  <rainbru@free.fr>

	* lib/router.js (adminNews): add a subscription to news collection.
	* server/fixtures.js: add an example news to test for publishing.
	* client/templates/admin/news.js (username): add conditionnal if
	using isn't logged in.

2016-10-19  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js (news): try to fix news list printing.

	* client/templates/admin/news.js (click #add): reset modal to
	default/empty values if added correctly.
	* .meteor/versions: update meteor packages.

2016-10-08  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js (Template.datepicker.rendered):
	autoload the datepicker JQuery widget.
	* client/templates/admin/news.html: datepicker has its own template.
	* .meteor/packages: add mizzao:jquery-ui dependency.
	* .meteor/versions: update meteor packages.

2016-09-30  Jérôme Pasquier  <rainbru@free.fr>

	* package.json: add native bcrypt package.

	* client/templates/admin/news.html (addNews): add date and author
	fields.

	* client/templates/admin/news.html: fix the add news table width.

	* .meteor/versions: update meteor packages.

2016-08-21  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.js (newsTypes): new helper returns
	news types array for AdminNewsType template.
	* client/templates/admin/news.html (AdminNewsType): new template to
	print each new type option.

2016-08-12  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/news.html (news helper): try to return
	actual news list but fail to print it.
	* client/templates/admin/news.js (click #add): handle the add news
	event with null string validation and add to collection.
	* client/templates/admin/news.html (addNews): fix the 'Homepage' typo
	and change the visibility field name from 'type'.
	* server/publish.js (news): publish all news.

2016-06-18  Jérôme Pasquier  <rainbru@free.fr>

	* lib/router.js: add router rule for admin/news.
	* client/templates/admin/news.js: add the JS helper for news
	administration.
	* client/templates/admin/news.html: add the HTML code to print the
	table.
	* client/includes/sidebar.html: added news administration link.

2016-05-04  Jérôme Pasquier  <rainbru@free.fr>

	* .meteor/versions: update meteor packages.

2016-03-22  Jérôme Pasquier  <rainbru@free.fr>

	* karma.conf.js: all blocks commentted out due to a ReferenceError
	with the require keyyword.
	* client/includes/news.js: try to implement the NewsType enumeration
	here.
	* lib/collections/collections.js: add the enws collection.
	* features/step_definitions/Given_I_am_a_new_user.js: all block
	commentted out due to a ReferenceError with the require keyyword.

2016-02-23  Jérôme Pasquier  <rainbru@free.fr>

	* features/step_definitions/Given_I_am_a_new_user.js: move old cucumber
	testing function here.

2016-02-16  Jérôme Pasquier  <rainbru@free.fr>

	* karma.conf.js: added to comply with travis.ci.

	* .scripts: add some files from xolvio/automated-testing-best-practices.
	* .travis.yml (node_js): remove v0.10, not supported by chimp.
	* travis.sh: not needed anymore.

	* features/guest-title.feature: switch from cucumber to chimp.

2015-08-31  Jérôme Pasquier  <rainbru@free.fr>

	* server/fixtures.js (addChilds): starting to read the first
	city (private/AFG.json).

	* private/_CONMEBOL.json: fixed extra comma in last childs item.

	* private/AFG.json: first content version.

	* ROADMAP: new geographies section to reset times.

2015-08-18  Jérôme Pasquier  <rainbru@free.fr>

	* server/fixtures.js: sorts logged arrays (for to-be-created one).

	* private/_UEFA.json: adds countries definitions.

	* private/_CONMEBOL.json: removes NZL to fix a collision with OFC.

2015-08-08  Jérôme Pasquier  <rainbru@free.fr>

	* .meteor/versions: meteor packages update.

2015-08-04  Jérôme Pasquier  <rainbru@free.fr>

	* server/fixtures.js (addChilds): adds an primitive anti-collision
	test for country code claimed by more than one country.

	* private/_OFC.json: adds countries definitions.

	* private/_CONMEBOL.json: adds countries definitions.
	* .meteor/versions: updated to Meteor 1.1.0.3 and xolvio:cucumber
	to 0.13.3.

2015-07-31  Jérôme Pasquier  <rainbru@free.fr>

	* private/_CONCACAF.json: fixes missing abbr and name in JSON.

	* server/fixtures.js (addChilds): implements a better error handling:
	all errors printed at the end.

	* server/fixtures.js (addChilds): shows correct parent code instead
	of always telling "error while parsing FIFA.json".
	* README.md: adds --settings settings.json to the meteor command.

	* private/_CONCACAF.json: adds CONCACAF definitions.

	* private/__FIFA.json: prefix with underscore(s) so we can have
	a country with CAF, AFC or OFC name.

2015-07-27  Jérôme Pasquier  <rainbru@free.fr>

	* tests/cucumber/features/guest-title.feature (register): trying
	to remove '/' (backslash) before URL to avoid the
	"Error: POST /session//url" error from travis-ci.

2015-07-27  Jérôme Pasquier  <rainbru@free.fr>

	* .travis.yml (env): add 'SELENIUM_BROWSER=firefox' definition.
	* tests/: rename current tests to usefull filenames.

2015-07-27  Jérôme Pasquier  <rainbru@free.fr>

	* tests/: tests files moved back to cucumber/ subdir.

2015-07-21  Jérôme Pasquier  <rainbru@free.fr>

	* tests/cucumber/: moved to a top-level directory.
	* .meteor/versions: updated cucumber version.

2015-07-21  Jérôme Pasquier  <rainbru@free.fr>

	* .meteor/versions: updated two meteor packages to fix the
	failure of travic-ci on all Node versions.

2015-07-14  Jérôme Pasquier  <rainbru@free.fr>

	* .meteor/versions: updated two meteor packages to fix the
	failure of travic-ci on Node v0.12.

2015-07-06  Jérôme Pasquier  <rainbru@free.fr>

	* tests/cucumber/features/sample.feature: removed intentional failure.
	* tests/settings.travis.json: moved to test/.
	* .meteor/versions: updated all meteor packages.

2015-07-06  Jérôme Pasquier  <rainbru@free.fr>

	* .travis.yml: Trying with the `script` command.

	* .travis.yml: Trying with a new env test command syntax.
	* .travis.yml: Trying to remove laika dependency.

	* travis.sh: now uses new settings.
	* tests/settings.travis.json: new admin settings for travis.

	* .travis.yml: try to fix travis build with 'sudo: required'.
	* .travis.yml: try travis as CI service.

2015-06-22  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geography.js (childs): childs list is now
	sorted.

	* client/templates/admin/geographies.js (getChildsNumber); chils
	number now correct.
	* server/fixtures.js (addChilds): recursively add geographies
	from private/ json assets.

2015-06-22  Jérôme Pasquier  <rainbru@free.fr>

	* private/CAF.json: finished.

	* server/fixtures.js: first try to load CAF.json.
	* client/templates/admin/geographies.js (getChildsNumber): fix
	a bug because ch was set to 1.
	* server/publish.js (geoParent): fix a bug when g is undefined.

2015-06-16  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geography.js (click .delete-child): removing
	a child geography works.

	* client/templates/admin/geographies.js: recursive childs number works.

2015-06-15  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geography.js (parentName): correctly prints
	the name of the parent geography.

	* client/templates/admin/geography.js (childs): returns all childs
	of the given parent.
	* client/templates/admin/geography.js (submit .child): correctly
	inserts geography object and set its parent.

	* ROADMAP: adds the browser's username/password thing.

2015-06-14  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geography.html: add childs UI.
	* client/templates/admin/geography.html: implements cities list.

2015-06-13  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geography.js (click .delete-prefix): deleting
	a prefix works.

	* client/templates/admin/geography.js (events): can't add a prefix
	if it already exists.

	* client/templates/admin/geography.html: prints prefix list.
	* client/helpers/array.js (addIndex): adds an index to a flat array.

2015-06-12  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geography.js: adding a prefix in the
	collection works.

2015-06-11  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geography.js: adds a basic helper.
	* client/templates/admin/geography.html: create template for individual
	geography management.
	* lin/router.js: create route for individual geography management.

2015-06-10  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geographies.js: adding geography works.

	* server/admin.js: now read values from settings.json file.

	* README.md: added a section on setiings.json file.

2015-06-02  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geographies.js: deleting geographies and
	generators works.

2015-06-01  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geographies.html: starts to handle
	competition generator.

2015-05-31  Jérôme Pasquier  <rainbru@free.fr>

	* client/includes/register.js (submit form): implements a basic
	email confirmation test.

	* client/templates/home.html: have a complete homepage.

2015-05-30  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/geographies.html: basic geographies
	handling.

2015-05-28  Jérôme Pasquier  <rainbru@free.fr>

	* client/templates/admin/names.html: add the lastnames table.
	* client/templates/admin/names.js: adding/removing lastnames works.

	* client/templates/profile.js (Template.profile.helpers): adds
	profile's username and email address.

	* client/templates/profile.html: add an admin indicator.

	* client/templates/loading.html: add a simple loading template.
	* lib/router.js (Router.configure): add the loading template rule.

	* client/templates/admin/names.js (Template.line.events): now we
	can delete a firstname.

	* client/templates/admin/names.js (Template.AdminNames.events): adding
	new firstname works with full validation.

2015-05-27  Jérôme Pasquier  <rainbru@free.fr>

	* client/layouts/layout.html: adds a sticky footer.

	* .js: added GNU headers fr AGPLv3 to all javascripts sources.
	* ./: added some GNU needed files.

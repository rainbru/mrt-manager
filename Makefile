CLI=node_modules/spacejam/bin/spacejam
DRV=cultofcoders:mocha
TXT=cultofcoders:mocha         # Doesn't work
SET=--settings test/settings.travis.json
COV=--settings test/settings.coverage.json
check:
	meteor test --full-app --driver-package=$(DRV) $(SET)

cli-check:
	MOCHA_REPORTER=console $(CLI) test --full-app --driver-package=$(TXT) $(SET)


coverage:
	$(CLI) test --full-app --coverage 'out_lcovonly out_coverage out_html' --driver-package=$(TXT)

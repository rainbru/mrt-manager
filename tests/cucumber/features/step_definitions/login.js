/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

(function () {

  'use strict';

  // You can include npm dependencies for support files in tests/cucumber/package.json
  var _ = require('underscore');

  module.exports = function () {

    // You can use normal require here, cucumber is NOT run in a Meteor context (by design)
    var url = require('url');

    this.Given(/^I am a new user$/, function () {
      // no callbacks! DDP has been promisified so you can just return it
      return this.server.call('reset'); // this.ddp is a connection to the mirror
    });

    this.When(/^I navigate to "([^"]*)"$/, function (relativePath, callback) {
	// WebdriverIO supports Promises/A+ out the box, so you can return that
	// too
	// this.browser is a pre-configured WebdriverIO + PhantomJS instance
	// process.env.ROOT_URL always points to the mirror
	this.client. 
            url(url.resolve(process.env.ROOT_URL, relativePath)). 
            call(callback);
    });

      this.Then(/^I should see the title "([^"]*)"$/,function (expectedTitle,
							       callback) {
	// you can use chai-as-promised in step definitions also
	// WebdriverIO chain-able promise magic
	  this.client.
	      waitForVisible('body *'). 
	      getTitle().should.become(expectedTitle).and.notify(callback);
    });

  };

})(); 

/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Returns a nomal distributed random number
 * in the form (\) between 0 and 1
 */
function normal()
{
    var u = Math.random();
    var v = Math.random();
    var r = u * u + v * v;
    if (r === 0 || r > 1){
	return normal();
    }
    var c = Math.sqrt(-2 * Math.log(r) / r);
    var m = u * c;
    if (m > 3)
	return normal();
    
    return m / 3;
}


/* Returns a gaussian distributed random number
 * in the form (/\) between 0 and 1
*/
function gaussian(){
    var u1, u2;  // uniformly distributed random numbers
    var w;       //  variance, then a weight
    var g1, g2;  // gaussian-distributed numbers

    do {
        u1 = 2 * Math.random() - 1;
        u2 = 2 * Math.random() - 1;
        w = u1*u1 + u2*u2;
    } while ( w >= 1 );

    w = Math.sqrt( (-2 * Math.log(w))  / w );
    g1 = u2 * w;

    if (g1 < -3 || g1 > 3){
	return gaussian();
    }

    return (g1 + 3) /6;
}

export {normal, gaussian};

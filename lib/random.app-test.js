/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {normal, gaussian} from './random.js';

if (Meteor.isClient) {
    describe('normal', function (){
	it("should return a number", function (){
	    var n = normal();
	    assert.ok(typeof n  === "number");
	})
	
	it("should return a number between 0 and 1", function (){
	    var n = normal();
	    assert.ok(n > 0 && n < 1);
	    
	})
    })
    
    describe('gaussian', function (){
	it("should return a number", function (){
	    var n = gaussian();
	    assert.ok(typeof n  === "number");
	})
	
	it("should return a number between 0 and 1", function (){
	    var n = gaussian();
	    assert.ok(n > 0 && n < 1);
	    
	})
    })

}

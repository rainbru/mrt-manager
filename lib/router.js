/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
});

Router.route('/',                  {name: 'home'});
Router.route('profile',            {name: 'profile'});
Router.route('register',           {name: 'register'});
Router.route('login',              {name: 'login'});
Router.route('overview',           {name: 'overview'});

Router.route('admin/dashboard', {
    name: 'adminDashboard' ,
    waitOn: function(){
	return [
	    Meteor.subscribe("fnames"),
	    Meteor.subscribe("lnames"),
	    Meteor.subscribe("allgeographies"),
	    Meteor.subscribe("teams"),
	    Meteor.subscribe("players")
	];
    }
});

Router.route('admin/names', {
    name: 'adminNames',
    waitOn: function(){
	return [
	    Meteor.subscribe("fnames"),
	    Meteor.subscribe("lnames")
	];
    }
});

Router.route('admin/geographies', {
    name: 'adminGeographies',
    waitOn: function(){
	return [
	    Meteor.subscribe("allgeographies"),
	    Meteor.subscribe("generators"),
	];
    }
});

Router.route('admin/geography/:_id', {
    name: 'adminGeography',
    waitOn: function(){
	var _id  = this.params._id;
	return [
	    Meteor.subscribe("geography", _id),
	    Meteor.subscribe("geoChilds", _id),
	    Meteor.subscribe("geoParent", _id),
	    Meteor.subscribe("teams"),
	];
    }
});

Router.route('admin/news', {
    name: 'adminNews',
    waitOn: function(){
	return [
	    Meteor.subscribe("news"),
	];
    }
});

Router.route('admin/team', {
    name: 'adminTeam',
    waitOn: function(){
	return [
	    Meteor.subscribe("teams"),
	    Meteor.subscribe("players"),
	];
    }
});

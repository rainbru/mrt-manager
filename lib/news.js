/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Give a string from a news type
Meteor.newsLib = {
    typeToString: function(newsType){
	iNewsType = parseInt(newsType); // Make sure the parameter is an int
	switch (iNewsType) {
	case 0:
	    return "Homepage";
	case 2:
	    return "Logged in";
	case 3:
	    return "Administrator";
	default:
	    return "Undefined news type"
	}
    },

    visibilityToString: function(newsVisibility){
	iNewsVis = parseInt(newsVisibility);// Make sure the parameter is an int
	switch (iNewsVis){
	case 0:
	    return "Shown";
	case 1:
	    return "Hidden";
	default:
	    return "Undefined visibility type";
	}
    }
}

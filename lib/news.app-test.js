/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import './news.js';

if (Meteor.isClient) {
    describe('typeToString', function (){
	it("should return a string", function (){
	    var n = Meteor.newsLib.typeToString(0);
	    assert.ok(typeof n  === "string");
	})
	
	it("typeToString(0) should return 'Homepage'", function (){
	    var n = Meteor.newsLib.typeToString(0);
	    assert.equal(n, "Homepage");
	})
    })

    describe('visibilityToString', function (){
	it("should return a string", function (){
	    var n = Meteor.newsLib.visibilityToString(0);
	    assert.ok(typeof n  === "string");
	})
	
	it("visibilityToString(0) should return 'Shown'", function (){
	    var n = Meteor.newsLib.visibilityToString(0);
	    assert.equal(n, "Shown");
	})
    })

}

/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

//import './player.js';

// This is the number of team in each level's league
var teamsPerLevel = 8;
var playersPerTeam = 20;

Meteor.TeamGenerator = {

    /** Return the number of league in a given level 
      *
      */
    leaguesByLevel: function(l){
	if (l < 1){
	    return 0;
	}
	if (l === 1){
	    return 1;
	}
	else if (l === 2 || l ===3 ){
	    return 2;
	}
	else if (l >= 4 && l <= 6 ){
	    return 4;
	}
	else {
	    return 8;
	}
    },

    /** Return the number of level to feed the league from level 1
      *
      */
    leagueNumber: function(level){
	var ret = 0;
	for (var i = 1; i <= level; ++i) {
	    ret += Meteor.TeamGenerator.leaguesByLevel(i);
	}
	return ret;
    },

    /** Returns the number of needed teams to feed given levels
      *
      */
    teamNumber: function(level){
	return Meteor.TeamGenerator.leagueNumber(level) * teamsPerLevel;
    },


    /** Generate a new team name from a country JSON object
      *
      * The teams array is the already created teams, used to check for
      * already existing names.
      *
      */
    teamName: function(jsonObject, teans){
	var p = jsonObject.prefixes[Math.floor(Math.random()*jsonObject.
					       prefixes.length)];
	var c = jsonObject.cities[Math.floor(Math.random()*jsonObject.
					     cities.length)];
	var s = jsonObject.suffixes[Math.floor(Math.random()*jsonObject.
					       suffixes.length)];

	// If no prefix, simply return with suffix
	if (jsonObject.prefixes.length == 0) {
	    return c + " " + s;
	}
	
	// If no suffix, simply return with prefix
	if (jsonObject.suffixes.length == 0) {
	    return p + " " + c;
	}
	
	// If both, choose randomly
	if (Math.random() < 0.5) {
	    return c + " " + s;
	} else {
	    return p + " " + c;
	}

    },

    /** Generate a new season for the given country JSON object
      *
      * @parentId is the country id in the database
      *
      * @return null if bad parameter, an interger if everything OK. This 
      *         integer represents the number of created records.
      *
      */
    newSeason: function(jsonObject, parentId){
	// Must take a country JSON file as argument
	if (!(jsonObject instanceof Object)){
	    return null;
	}

	// Generate all teams
	var nbTeams = Meteor.TeamGenerator.teamNumber(jsonObject.config.levels);

	var created = 0; // The number of created teams
	var level = 1;   // The team level
	var group = 1;   // the team group
	var createdInGroup = 0;
	
	var teams = Array.new;
	for (var i=1; i <= nbTeams; i++) {
	    // Generate one team
	    console.log("Generating team with parentId : " + parentId);
	    var name = Meteor.TeamGenerator.teamName(jsonObject, teams);

	    // Ensure we don't have multiple teams with the same name
	    id = Teams.insert({
		label: name,
		numeral: Meteor.TeamGenerator.existingTeamName(name),
		parent: parentId,
		level: level,
		group: group
		
	    });

	    // Handle group and level
	    created = created + 1;
	    createdInGroup = createdInGroup + 1;
	    if (createdInGroup == teamsPerLevel) {
		createdInGroup = 0;
		group = group + 1;
		if (group > Meteor.TeamGenerator.leaguesByLevel(level)) {
		    group = 1;
		    level = level + 1;
		}
	    }

	    // Now, generate players for this new team
	    for (var p=1; p <= playersPerTeam; p++) {
		Meteor.PlayerGenerator.newPlayer(id, jsonObject);
		p = p + 1;
	    }

	}
	
	return created;
    },

    /** Returns the number of team name already used */
    existingTeamName: function(name){
	return Teams.find({label: name}).count();
    }
    
}

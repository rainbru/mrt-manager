/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {getRandomHeight} from "./../player.js";

Meteor.PlayerGenerator = {

    /** Generate a new player for the given team/country pair
      *
      */
    newPlayer: function(teamId, jsonObject) {
	// Must take a country JSON file as argument
	if (!(jsonObject instanceof Object)){
	    return null;
	}
	
	var n1 = jsonObject.firstnames[Math.floor(Math.random()*jsonObject.
						firstnames.length)];
	var n2 = jsonObject.lastnames[Math.floor(Math.random()*jsonObject.
					      lastnames.length)];
	var name = n1 + ' ' + n2;

	// Should avoid 2 players with the same name
	var found = Players.find({name: name}).count();
	while ( found > 0) {
	    console.log("==> Player already exist, searching for new name");
	    var n3 = jsonObject.firstnames[Math.floor(Math.random()*jsonObject.
						      firstnames.length)];
	    var n1 = n1 + "-" + n3;
	    var name = n1 + ' ' + n2;
	    var found = Players.find({name: name}).count();
	}


	var p = {name: name, height: getRandomHeight()};
	Players.insert(p);
	console.log("Generated new player for team " + id, p);
	
    }
}

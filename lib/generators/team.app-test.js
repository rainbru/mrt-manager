/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import './team.js';

if (Meteor.isServer) {
    describe('newSeason', function (){
	it("should return null if not a JSON object as argument", function (){
	    var n = Meteor.TeamGenerator.newSeason(0, 0);
	    assert.ok(n  === null);
	    Teams.remove({});
	})

	it("should return null with a string argument", function (){
	    var n = Meteor.TeamGenerator.newSeason("aaa", 0);
	    assert.ok(n  === null);
	    Teams.remove({});
	})

	it("should return something with a JSON object argument", function (){
	    myjson = JSON.parse(Assets.getText("AFG.json"));
	    console.log(myjson);
	    var n = Meteor.TeamGenerator.newSeason(myjson, 0);
	    assert.ok(n  != null);
	    Teams.remove({});
	})

	it("should create 24 teams", function (){
	    myjson = JSON.parse(Assets.getText("AFG.json"));
	    console.log(myjson);
	    var n = Meteor.TeamGenerator.newSeason(myjson, 0);
	    assert.equal(n, 24);
	    Teams.remove({});
	})

	it("should have added 24 teams to collection", function (){
	    Meteor.TeamGenerator.newSeason(myjson, 0);
	    var teams = Teams.find({}).fetch();
	    assert.equal(teams.length, 24);
	    Teams.remove({});
	})

	it("should have a level property in created team", function (){
	    Meteor.TeamGenerator.newSeason(myjson, 0);
	    var team = Teams.findOne({});
	    assert.notEqual(team.level, undefined);
	    Teams.remove({});
	})

	it("should have a group property in created team", function (){
	    Meteor.TeamGenerator.newSeason(myjson, 0);
	    var team = Teams.findOne({});
	    assert.notEqual(team.group, undefined);
	    Teams.remove({});
	})

	it("should have create 8 teams in level 1", function (){
	    Meteor.TeamGenerator.newSeason(myjson, 0);
	    var teams = Teams.find({level: 1}).fetch();
	    assert.equal(teams.length, 8);
	    Teams.remove({});
	})

	it("should have create 16 teams in level 2", function (){
	    Meteor.TeamGenerator.newSeason(myjson, 0);
	    var teams = Teams.find({level: 2}).fetch();
	    assert.equal(teams.length, 16);
	    Teams.remove({});
	})

	it("should have create 8 teams in level 2 group 1", function (){
	    Meteor.TeamGenerator.newSeason(myjson, 0);
	    var teams = Teams.find({level: 1, group: 1}).fetch();
	    assert.equal(teams.length, 8);
	    Teams.remove({});
	})

	
	
    })

    describe('leaguesByLevel', function (){
	it("should return 0", function (){
	    assert.equal(Meteor.TeamGenerator.leaguesByLevel(0), 0);
	})
	it("should return 1", function (){
	    assert.equal(Meteor.TeamGenerator.leaguesByLevel(1), 1);
	})
	it("should return 2", function (){
	    assert.equal(Meteor.TeamGenerator.leaguesByLevel(2), 2);
	})
	it("should return 2", function (){
	    assert.equal(Meteor.TeamGenerator.leaguesByLevel(3), 2);
	})
	it("should return 4", function (){
	    assert.equal(Meteor.TeamGenerator.leaguesByLevel(5), 4);
	})
	it("should return 8", function (){
	    assert.equal(Meteor.TeamGenerator.leaguesByLevel(15), 8);
	})

	
    })

    describe('leagueNumber', function (){
	it("should return 1 leagues", function (){
	    assert.equal(Meteor.TeamGenerator.leagueNumber(1), 1);
	})
	it("should return 3 leagues", function (){
	    assert.equal(Meteor.TeamGenerator.leagueNumber(2), 3);
	})
	it("should return 3 leagues", function (){
	    assert.equal(Meteor.TeamGenerator.leagueNumber(3), 5);
	})
    })

    describe('teamNumber', function (){
	it("should return 8 teams", function (){
	    assert.equal(Meteor.TeamGenerator.teamNumber(1), 8);
	})
	it("should return 24 teams", function (){
	    assert.equal(Meteor.TeamGenerator.teamNumber(2), 24);
	})
	it("should return 40 teams", function (){
	    assert.equal(Meteor.TeamGenerator.teamNumber(3), 40);
	})
	it("should have many many teams for 12 levels", function (){
	    assert.equal(Meteor.TeamGenerator.teamNumber(12), 520);
	})
    })

    describe('teamName', function (){
	var json = {
	    "cities": [ "Kabul"]
	    }
		    
	it("should return a prefixed team name", function (){
	    json.prefixes = [ "PRE" ];
	    json.suffixes = [];
	    assert.equal(Meteor.TeamGenerator.teamName(json), "PRE Kabul");
	})

	it("should return a suffixed team name", function (){
	    json.prefixes = [];
	    json.suffixes = [ "SUF" ];
	    assert.equal(Meteor.TeamGenerator.teamName(json), "Kabul SUF");
	})

	it("should return a both prefixed and suffixed team names", function (){
	    json.prefixes = [ "PRE" ];
	    json.suffixes = [ "SUF" ];

	    // Generate 100 team names
	    var names = [];
	    for (var i=1; i< 100; i++) {
		names.push(Meteor.TeamGenerator.teamName(json));
	    }

	    // Get unique generated names
	    var unames = names.filter((v, i, a) => a.indexOf(v) === i);
	    
	    assert.equal(unames.length, 2);
	})
    })
    
    describe('existingTeamName', function (){
	it("should return 0", function (){
	    var name = "F.C. nothing";
	    assert.equal(Meteor.TeamGenerator.existingTeamName(name), 0);
	})
	it("should return 1", function (){
	    var name = "FC.nothing";
	    id = Teams.insert({ label: name });
	    assert.equal(Meteor.TeamGenerator.existingTeamName(name), 1);
	})

	it("should return 1", function (){
	    var name = "nothing A.S.";
	    id = Teams.insert({ label: name });
	    id = Teams.insert({ label: name });
	    id = Teams.insert({ label: name });
	    id = Teams.insert({ label: name });
	    id = Teams.insert({ label: name });
	    id = Teams.insert({ label: name });
	    id = Teams.insert({ label: name });
	    id = Teams.insert({ label: name });
	    assert.equal(Meteor.TeamGenerator.existingTeamName(name), 8);
	})

    })

    
}

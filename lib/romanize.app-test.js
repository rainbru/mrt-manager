/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


import './romanize.js';

if (Meteor.isClient) {
    describe('romanize', function (){
	it("should return nothing with 0", function (){
	    assert.ok(romanize(0)  === "");
	})
	it("should return 1 in roman number", function (){
	    assert.ok(romanize(1)  === "I");
	})
	it("should return 5 in roman number", function (){
	    assert.ok(romanize(5)  === "V");
	})
	it("should return 10 in roman number", function (){
	    assert.ok(romanize(10)  === "X");
	})
	it("should return 20 in roman number", function (){
	    assert.ok(romanize(20)  === "XX");
	})
	it("should return 3999 roman number", function (){
	    assert.ok(romanize(3999)  === "MMMCMXCIX");
	})
    })
}


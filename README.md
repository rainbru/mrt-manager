Mrt-manager
===========

<tt>mrt-manager</tt> is a soccer management game written in meteor.

Installation
------------

You have to install mongodb and meteor first :

Install mongodb https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/#install-mongodb-community-edition

```
curl https://install.meteor.com/ | sh
```

Then, you can clone and run :

```
git clone https://github.com/rainbru/mrt-manager.git
cd mrt-manager
meteor --settings settings.json
```

Unit tests
----------

This application comes with unit tests. To run them

	sudo apt-get install curl
	curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
	sudo apt-get install nodejs
	sudo npm install spacejam

Then, to run them :

	make check

Settings
--------

To configure analytics and admin account , you have to pass your GA ID and
your administration credentials in a *settings.json* file :

```json
{
    "public": {
		"analyticsSettings": {
			"Google Analytics" : {"trackingId": "UA-XXXXXXXX-X"}
		}
    },
    "private": {
		"admin": {
			"email": "YOUR-EMAIL-HERE",
			"password": "YOUR-PASSWORD",
			"profile": "YOUR-FULLNAME"
		}
    }
}
```

Then, you'll need to pass it to run or deploy the application
```
meteor --settings settings.json
deploy APPLICATION-NAME.meteor.com --settings settings.json
```

Troubleshooting
---------------


## mongo exit code 1

When running `meteor run`, you may have the following error message :

	Unexpected mongo exit code 1. Restarting.     
	Unexpected mongo exit code 1. Restarting.     
	Unexpected mongo exit code 1. Restarting.     
	Unexpected mongo exit code 1. Restarting.     
	Unexpected mongo exit code 1. Restarting.     
	Can't start Mongo server.                     
	MongoDB failed global initialization
	
This issue appears because mongodb can't find the locale it needs. You need
to install it. Edit the */etc/locale.gen* as root
and uncomment the following line :

	en_US.UTF-8 UTF-8
	
Then execute the following command as root :

	locale-gen

## mongo exit code 62

If you got a bunch of :

	Unexpected mongo exit code 62. Restarting.    
	Unexpected mongo exit code 62. Restarting.    
	Unexpected mongo exit code 62. Restarting.    
	Unexpected mongo exit code 62. Restarting.  
	
Try this :

	mongodump 
	meteor reset
	mongorestore 


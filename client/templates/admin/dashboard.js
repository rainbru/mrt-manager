/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

Template.AdminDashboard.helpers({
    stats: function(){
	var namesCount = Firstnames.find().count() 
	    + Lastnames.find().count();
	var usersCount = Meteor.users.find().count();
	var geoCount   = Geographies.find().count();
	var teamCount   = Teams.find({}).count();
	var playCount   = Players.find({}).count();

	return [
	    {label: "Firstnames/Lastnames", count: namesCount, url: "names"},
	    {label: "Registered users",   count: usersCount, url: "users"},
	    {label: "Geography objects", count: geoCount, url: "geographies"},
	    {label: "Teams",       count: teamCount, url: "geographies"},
	    {label: "Players",     count: playCount, url: "geographies"}
	    
	];
    }
});

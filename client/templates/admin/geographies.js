/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

if (Meteor.isClient) {

    Template.AdminGeographies.helpers({
	geographies: function(){
	    var ret = [];
	    var cursor = Geographies.find({parent: null}, 
					  {sort: [[ "label", "asc"]]});
	    var idx = 0;
            cursor.forEach(function(row){
		var ch = getChildsNumber(row._id, row.label);
		ret.push({num: ++idx, label: row.label, 
			  childs: ch, id: row._id});
            }); 

	    return ret;
	},
	generators: function(){
	    var ret = [];
	    var cursor = Generators.find({parent: null}, 
					  {sort: [[ "label", "asc"]]});
	    var idx = 0;
	    cursor.forEach(function (row) {
		ret.push({num: ++idx, label: row.label, id: row._id,
			  period: row.periodicity,input:  row.input, 
			  output: row.output, created: false});
            }); 

	    return ret;
	}

    });

    Template.geo.events({
	"click .delete": function (evt, template) {
	    evt.preventDefault();
	    
            var id = $(evt.currentTarget).attr("id");
	    Geographies.remove(id, function(err){ 
		if (err){
		    throwError("Can't remove geography : " + err);
		}
	    });
	}
    });

    Template.generator.events({
	"click .delete": function (evt, template) {
	    evt.preventDefault();

	    var id = $(evt.currentTarget).attr("id");
	    Generators.remove(id, function(err){ 
		if (err){
		    throwError("Can't remove generator : " + err);
		}
	    });
	}
    });

    Template.AdminGeographies.events({
	"submit .geography": function (event, template) {
	    event.preventDefault();

            var text = template.find("input[name=geography]");
	    var f = capitalizeFirstLetter(text.value);
	    
	    // Form validation
	    if (!f){
		throwError("Can't add an empty geography");
		return false;
	    }

	    // Add to database
	    Geographies.insert({label: f}, function(err) { 
		if (!err){
		    text.value = "";
		}
		else{
		    throwError("Can't insert firstname : " + err);
		}
	    });
	}
    });
		
}

/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

if (Meteor.isClient) {
    Template.AdminNews.helpers({
	news: function(){
	    
	    var ret = [];
	    var cursor = News.find({});
	    var idx = 0;
	    cursor.forEach(function (row) {
		ret.push({title: row.title,
			  type: Meteor.newsLib.typeToString(row.type),
			  visibility: Meteor.newsLib.visibilityToString(row.visibility),
			  id: row._id});
		
            }); 
	    return ret;
	},
   
    });

    Template.addNews.helpers({
	newsTypes: function (){
	    return [{label:"Homepage", value: 0, code: "H"},
		    {label: "Logged in users", value: 2, code: "LI"},
		    {label: "Administrator", value: 3, code: "A"}
		   ];
	},
	today: function (){
	    return new Date().toLocaleString('en');
	},
	username: function(){
	    if (Meteor.user()){
		return Meteor.user().profile["name"];
	    }
	    else {
		return "Guest";
	    }
	}
    });
    
    Template.addNews.events({
	'click #add': function(event, template){
	    event.preventDefault();
	    
	    var title = template.find("input[name=title]").value;
	    title = capitalizeFirstLetter(title);
	    var content = template.find("textarea[name=content]").value;
	    content = capitalizeFirstLetter(content);

	    if (!title){
		throwError("News title can't be null");
		return false;
	    }

	    if (!content){
		throwError("News content can't be null");
		return false;
	    }

	    var type = parseInt(template.find("select[name=type]").value);
	    var visibility = parseInt(template.find("select[name=visibility]")
				      .value);
	    var author = Template.addNews.__helpers.get('username').call();
	    var date = template.find("input[name=date]").value;
	    
	    News.insert({
		title: title,
		date: date,
		author: author,
		type: type,
		visibility: visibility,
		content: content
	    },function(err) { 
		if (!err){
		    // Reset modal values
		    template.find("input[name=title]").value = "";
		    template.find("textarea[name=content]").value = "";
		    template.find("select[name=type]").value = 0;
		    template.find("select[name=visibility]").value = 0;
		}
		else{
		    throwError("Can't insert news : " + err);
		}
	    });

	    // Hide the bootstrap modal
	    $('#myModal').modal('hide');
	}
    });

    Template.datepicker.rendered=function(){
	this.$('#datepicker').datepicker();
	this.$('#datepicker').datepicker('setDate', new Date());
    };

    Template.AdminNewsEach.events({
	"click .delete": function (evt, template) {
	    evt.preventDefault();

	    var id = $(evt.currentTarget).attr("id");
	    var title = News.findOne({ _id: id }).title;

	    if (confirm("Are you sure yuo want to delete news titled '" +
			title + "' ?")){
		News.remove(id, function(err){ 
		    if (err){
			throwError("Can't remove news : " + err);
		    }
		});
	    }
	},
	
	"click .modify": function (evt, template) {
	    evt.preventDefault();

	    // Get the id and the corresponding News
	    var id = $(evt.currentTarget).attr("id");
	    var n = News.findOne({ _id: id });
	    
	    // Modify dialog content
	    $('#title').val(n.title);
	    $('#content').val(n.content);
	    $('#date').val(n.date);
	    $('#author').val(n.author);
	    
	    // Preselect type and visibility combos
	    $("[name=type]").val(n.type); 
	    $("[name=visibility]").val(n.visibility);

	    // Set the data id, to modify the right news in #update event
	    $('#update').data('data-news-id', id);

	    // then, show the dialog
	    $('#modifyModal').modal('show');
	    $('#title').focus();

	}
    });

    Template.modifyNews.helpers({
	newsTypes: function (){
	    return types = [{label:"Homepage", value: 0, code: "H"},
			    {label: "Logged in users", value: 2, code: "LI"},
			    {label: "Administrator", value: 3, code: "A"}
			   ];
	}
    });

    Template.modifyNews.events({
	"click #update": function (evt, template) {
	    evt.preventDefault();

	    // evt.currentTarget is the update button!
	    // The id is set in "click .modify"
	    var id = $(evt.currentTarget).data("data-news-id");
	    
	    var title = template.find("input[name=title]").value;
	    title = capitalizeFirstLetter(title);
	    var content = template.find("textarea[name=content]").value;
	    content = capitalizeFirstLetter(content);

	    if (!title){
		throwError("News title can't be null");
		return false;
	    }

	    if (!content){
		throwError("News content can't be null");
		return false;
	    }

	    var type = parseInt(template.find("select[name=type]").value);
	    var visibility = parseInt(template.find("select[name=visibility]")
				      .value);
	    var author = Template.addNews.__helpers.get('username').call();
	    var date = template.find('#date').value;

	    News.update(id, { $set: {
		title: title,
		date: date,
		author: author,
		type: type,
		visibility: visibility,
		content: content
	    }},function(err, updated) { 
		if (!err){
		    // Hide the bootstrap modal
		    $('#modifyModal').modal('hide');
		}
		else{
		    throwError("Can't update news : " + err);
		}
	    });

	}});

    
    // Trying to set focus to Title : this doesn't work
    Template.modifyNews.onRendered(function(){
	this.autorun(function(){
	    Tracker.afterFlush(function(){
		$('#title').focus();
	    });
	});
    });

    
}

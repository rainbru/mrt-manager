/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

Template.AdminGeography.helpers({

    geo: function(){
	return Geographies.findOne(Router.current().params._id);
    },
    childs: function(){
	var id = Router.current().params._id;
	var cs = Geographies.find({parent: id}, {sort: [[ "label", "asc"]]});

	var css = []; // Will contain all childs

	console.log("Logging all teams");
	console.log(Teams.find({}).fetch());
	
	var ch = cs.count();
	cs.forEach(function (row) {
	    a = {}
	    console.log(row);
	    a['id'] = row._id;
	    a['label'] = row['label'];

	    console.log("Searching for childs with parentId " + row._id);
	    a['teams'] = getTeamsNumber(row._id); //, row.label);
	    css.push(a);
	}); 
	return css;
    },
    teams: function() {
	var id = Router.current().params._id;
	var t= Teams.find({parent: id}).fetch();
	t.forEach(function(team){
	    team.romanized = teamRomanizedName(team);
	});
	console.log("Romanized ?");
	console.log(t);
	return t;
    },
    parentName: function(parentId){
	if (parentId == null){
	    return Spacebars.SafeString("<em>None</em>");
	}
	else{
	    var geo = Geographies.findOne(parentId);
	    return geo.label;
	}
    }
});

Template.AdminGeography.events({
    "submit .prefix": function (event, template) {
	event.preventDefault();

	var text = template.find("input[name=prefix]");
	var f = capitalizeFirstLetter(text.value);
	    
	// Form validation
	if (!f){
	    throwError("Can't add an empty prefix");
	    return false;
	}
	
	var geo = Geographies.findOne(Router.current().params._id);

	// Test if the prefix already exists
	if (geo.prefixes){ // Can get an undefined prefixes property
	    if (geo.prefixes.indexOf(f) > -1){
		throwError("This prefix already exists");
		return false;
	    }
	}

	var id = Router.current().params._id;
	if (Geographies.update({ _id: id },{ $push: { prefixes: f }})){
	    text.value = "";
	}
	else {
	    throwError("Can't add prefix");
	}
    },
    "submit .city": function (event, template) {
	event.preventDefault();

	var text = template.find("input[name=city]");
	var f = capitalizeFirstLetter(text.value);
	    
	// Form validation
	if (!f){
	    throwError("Can't add an empty city");
	    return false;
	}
	
	var geo = Geographies.findOne(Router.current().params._id);

	// Test if the prefix already exists
	if (geo.cities){ // Can get an undefined cities property
	    if (geo.cities.indexOf(f) > -1){
		throwError("This city already exists");
		return false;
	    }
	}
	var id = Router.current().params._id;
	if (Geographies.update({ _id: id },{ $push: { cities: f }})){
	    text.value = "";
	}
	else {
	    throwError("Can't add city");
	}
    },
    "click .delete-prefix": function (evt, template) {
	evt.preventDefault();
	
	var id = Router.current().params._id;
	var prefix = $(evt.currentTarget).attr("id");

	Geographies.update({_id: id}, {'$pull': {"prefixes": prefix}})
    },
    "click .delete-city": function (evt, template) {
	evt.preventDefault();
	
	var id = Router.current().params._id;
	var city = $(evt.currentTarget).attr("id");

	Geographies.update({_id: id}, {'$pull': {"cities": city}})
    },
    "click .delete-child": function (evt, template) {
	evt.preventDefault();
	
	var id = $(evt.currentTarget).attr("id");

	Geographies.remove(id, function(err){ 
	    if (err){
		throwError("Can't remove child : " + err);
	    }
	});
    },
    "submit .child": function (event, template) {
	event.preventDefault();

	var id   = Router.current().params._id;
	var text = template.find("input[name=child]");
	var f    = capitalizeFirstLetter(text.value);

	Geographies.insert({
            label: f,
	    parent: id
	});
	text.value = "";
    }


});

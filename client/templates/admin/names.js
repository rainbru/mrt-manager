/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

if (Meteor.isClient) {
    Template.AdminNames.helpers({
	firstnames: function(){
	    var ret = [];
	    var cursor = Firstnames.find({}, {sort: [[ "label", "asc"]]});
	    var idx = 0;
	    cursor.forEach(function (row) {
		ret.push({num: ++idx, label: row.label, id: row._id});
            }); 

	    return ret;
	},

	lastnames: function(){
	    var ret = [];
	    var cursor = Lastnames.find({}, {sort: [[ "label", "asc"]]});
	    var idx = 0;
	    cursor.forEach(function (row) {
		ret.push({num: ++idx, label: row.label, id: row._id});
            }); 
	    return ret;
	}
    });


    Template.AdminNames.events({
	"submit .first": function (event, template) {
	    event.preventDefault();

	    var text = template.find("input[name=firstname]");
	    var f = capitalizeFirstLetter(text.value);
	    
	    // Form validation
	    if (!f){
		throwError("Can't add an empty firstname");
		return false;
	    }

	    if (Firstnames.find({label: f}).count() > 0){
		throwError("This firstname already exists!");
		return false;
	    }

	    // Add to database
	    Firstnames.insert({label: f}, function(err) { 
		if (!err){
		    text.value = "";
		}
		else{
		    throwError("Can't insert firstname : " + err);
		}
	    });
	},
	"submit .last": function (event, template) {
	    event.preventDefault();

	    var text = template.find("input[name=lastname]");
	    var f = capitalizeFirstLetter(text.value);
	    
	    // Form validation
	    if (!f){
		throwError("Can't add an empty lastname");
		return false;
	    }

	    if (Lastnames.find({label: f}).count() > 0){
		throwError("This lastname already exists!");
		return false;
	    }

	    // Add to database
	    Lastnames.insert({label: f}, function(err) { 
		if (!err){
		    text.value = "";
		}
		else{
		    throwError("Can't insert firstname : " + err);
		}
	    });
	},

    });

    Template.firstline.events({
	"click .delete": function (evt, template) {
	    evt.preventDefault();

	    var id = $(evt.currentTarget).attr("id");
	    Firstnames.remove(id, function(err){ 
		if (err){
		    throwError("Can't remove firstname : " + err);
		}
	    });
	}
    });

    Template.lastline.events({
	"click .delete": function (evt, template) {
	    evt.preventDefault();

	    var id = $(evt.currentTarget).attr("id");
	    Lastnames.remove(id, function(err){ 
		if (err){
		    throwError("Can't remove lastname : " + err);
		}
	    });
	}
    });

}

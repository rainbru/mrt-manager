/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import './errors.js';

describe('Errors helper', function (){
    it("should throw an error without message", function (){
	expect(throwError).to.throw();
    });

    it("should throw an error if message isn't a string", function (){
	expect(() => throwError(121)).to.throw();
    });
    
    it("must add the message to Errors collection", function (){
	var l = Errors.find().count();
	throwError("message");
	assert.ok(Errors.find().count() === l + 1)
    });
});

/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Get the childs number of the given geometry */
getChildsNumber =  function (parentId) {
    console.log("Getting child numbers for ID ", parentId);
    console.log(Geographies.find({parent: parentId}).fetch())
    return Geographies.find({"parent": parentId}).count();
};

/** Get the childs number of the given geometry */
getTeamsNumber =  function (parentId) {
    console.log("Getting child numbers for ID ", parentId);
    console.log(Teams.find({parent: parentId}).fetch())
    return Teams.find({"parent": parentId}).count();
};

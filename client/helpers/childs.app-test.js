/*
 *  mrt_manager - A soccer management game written in meteor.
 *  Copyright (C) 2015-2018  Jérôme Pasquier
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import './childs.js';

describe('Childs helper', function (){
    it("should correctly get childs number for missing parent", function (){
	expect(getChildsNumber(0) == 0);
    });

    it("should correctly get childs number", function (){
	Geographies.insert({parent: 7});
	expect(getChildsNumber(7) == 1);
    });
});

describe('TeamsNumber helper', function (){
    it("should correctly get childs number for missing parent", function (){
	expect(getTeamsNumber(0) == 0);
    });

        it("should correctly get childs number", function (){
	Teams.insert({parent: 7});
	expect(getTeamsNumber(7) == 1);
    });

});
